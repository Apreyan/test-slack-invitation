from test.util.TestUtil import wait_element_to_be_clickable
from test.util.WebdriverFactory import get_webdriver
from selenium.webdriver.common.by import By


def click_create_google_account():
    wait_element_to_be_clickable("[role='presentation'] [role='presentation'] div[role='button']")
    create_google_account = get_webdriver().find_elements(By.CSS_SELECTOR, "[role='presentation'] [role='presentation'] div[role='button']")[1]
    create_google_account.click()


def type_email_gmail(email):
    wait_element_to_be_clickable("input[type='email']")
    email_element = get_webdriver().find_element(By.CSS_SELECTOR, "input[type='email']")
    email_element.send_keys(email)


def type_password_gmail(password):
    wait_element_to_be_clickable("input[type='password']")
    password_element = get_webdriver().find_element(By.CSS_SELECTOR, "input[type='password']")
    password_element.send_keys(password)


def click_next_username():
    wait_element_to_be_clickable("[id='identifierNext']")
    next_username = get_webdriver().find_element(By.CSS_SELECTOR, "[id='identifierNext']")
    next_username.click()


def click_next_password():
    wait_element_to_be_clickable("[id='idPasswordNext']")
    next_password = get_webdriver().find_element(By.CSS_SELECTOR, "[id='idPasswordNext']")
    next_password.click()