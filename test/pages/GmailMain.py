from selenium.webdriver.support.wait import WebDriverWait

from test.util.TestUtil import wait_element_to_be_clickable, wait_for_page_to_load
from test.util.WebdriverFactory import get_webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC


def select_email():
    wait_element_to_be_clickable("[draggable='true']")
    email = get_webdriver().find_elements(By.CSS_SELECTOR, "[draggable='true']")[0]
    email.click()
    WebDriverWait(get_webdriver(), 30).until(
        EC.presence_of_element_located((By.CSS_SELECTOR, "a[href^='https://join.slack.com']")))
    # wait_element_to_be_clickable("a[href^='https://join.slack.com']")


def click_join_button():
    wait_for_page_to_load()
    # wait_element_to_be_clickable("a[href^='https://join.slack.com']")
    WebDriverWait(get_webdriver(), 30).until(
        EC.presence_of_element_located((By.CSS_SELECTOR, "a[href^='https://join.slack.com']")))

    join_button = get_webdriver().find_element(By.CSS_SELECTOR, "a[href*='https://join.slack.com']")

    for x in range (0, 10):
        try:
            join_button.click()
            break
        except:
            join_button = get_webdriver().find_element(By.CSS_SELECTOR, "a[href*='https://join.slack.com']")
