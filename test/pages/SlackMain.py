from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait

from test.util.TestUtil import wait_element_to_be_clickable
from test.util.WebdriverFactory import *


def click_invite_people():
    wait_element_to_be_clickable("[data-qa-channel-sidebar-link-id='Vinvites']")
    invite_people = get_webdriver().find_element(By.CSS_SELECTOR, "[data-qa-channel-sidebar-link-id='Vinvites']")
    invite_people.click()


def escape_welcome():
    wait_element_to_be_clickable("[id='coachmark_interior'] a[class='coachmark_close_btn']")
    welcome_balloon_close = get_webdriver().find_element(By.CSS_SELECTOR, "[id='coachmark_interior'] a[class='coachmark_close_btn']")
    welcome_balloon_close.click()



def send_message_by_name(user_name, message):
    wait_element_to_be_clickable("[aria-label^='" + user_name + "']")
    user = get_webdriver().find_element(By.CSS_SELECTOR, "[aria-label^='" + user_name + "']").click()
    wait_element_to_be_clickable("[data-qa='message_input'] [role='textbox']")
    message_input = get_webdriver().find_element(By.CSS_SELECTOR, "[data-qa='message_input'] [role='textbox']")
    message_input.send_keys(message)


def get_message_by_name(user_name):
    wait_element_to_be_clickable("[aria-label^='" + user_name + "']")
    get_webdriver().find_element(By.CSS_SELECTOR, "[aria-label^='" + user_name + "']").click()
    wait_element_to_be_clickable("[data-qa='message_container'] [data-qa='message-text']")
    message_container = get_webdriver().find_element(By.CSS_SELECTOR, "[data-qa='message_container'] [data-qa='message-text']")
    return message_container.text


def sign_out():
    wait_element_to_be_clickable("[id='team_menu_user']")
    get_webdriver().find_element(By.CSS_SELECTOR, "[id='team_menu_user']").click()
    wait_element_to_be_clickable("[id='logout']")
    get_webdriver().find_element(By.CSS_SELECTOR, "[id='logout']").click()
    WebDriverWait(get_webdriver(), 30).until(
        EC.url_contains( ".slack.com/signout/done"))