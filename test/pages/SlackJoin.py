from selenium.webdriver.common.by import By
from test.util.TestUtil import wait_element_to_be_clickable
from test.util.WebdriverFactory import *


def type_full_name_join(full_name_string):
    wait_element_to_be_clickable("input[name='full_name']")
    full_name = get_webdriver().find_element(By.CSS_SELECTOR, "input[name='full_name']")
    full_name.send_keys(full_name_string)


def type_password_join(password_string):
    wait_element_to_be_clickable("input[name='password']")
    password = get_webdriver().find_element(By.CSS_SELECTOR, "input[name='password']")
    password.send_keys(password_string)


def click_create_account():
    wait_element_to_be_clickable("[data-qa='submit_button']")
    create_account = get_webdriver().find_element(By.CSS_SELECTOR, "[data-qa='submit_button']")
    create_account.click()


def skip_invitations():
    wait_element_to_be_clickable("[id='secondary_btn']")
    skip_invitations = get_webdriver().find_element(By.CSS_SELECTOR, "[id='secondary_btn']")
    skip_invitations.click()
