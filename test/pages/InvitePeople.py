from selenium.webdriver.common.by import By
from test.util.TestUtil import wait_element_to_be_clickable
from test.util.WebdriverFactory import *


def type_email_by_index(index, email):
    wait_element_to_be_clickable("[id='invite_" + index + "'] [name='email_address']")
    email_input = get_webdriver().find_element(By.CSS_SELECTOR, "[id='invite_" + index + "'] [name='email_address']")
    email_input.send_keys(email)


def click_invite():
    wait_element_to_be_clickable("[id='admin_invites_submit_btn']")
    invite = get_webdriver().find_element(By.CSS_SELECTOR, "[id='admin_invites_submit_btn']")
    invite.click()

