from selenium.webdriver.common.by import By
from test.util.TestUtil import wait_element_to_be_clickable
from test.util.WebdriverFactory import *

def type_username(username_string):
    wait_element_to_be_clickable("[id='email']")
    username = get_webdriver().find_element(By.CSS_SELECTOR, "[id='email']")
    username.send_keys(username_string)


def type_password_slack(password_string):
    wait_element_to_be_clickable("[id='password']")
    password = get_webdriver().find_element(By.CSS_SELECTOR, "[id='password']")
    password.send_keys(password_string)


def click_signin():
    wait_element_to_be_clickable("[id='signin_btn']")
    signin_button = get_webdriver().find_element(By.CSS_SELECTOR, "[id='signin_btn']")
    signin_button.click()