from test.pages.GmailLogin import *
from test.pages.GmailMain import select_email, click_join_button
from test.pages.InvitePeople import click_invite, type_email_by_index
from test.pages.SlackJoin import type_full_name_join, type_password_join, click_create_account, skip_invitations
from test.pages.SlackLogin import *
from test.pages.SlackMain import click_invite_people, send_message_by_name, get_message_by_name, escape_welcome, \
    sign_out
from test.util.TestUtil import wait_for_page_to_load


class TestInvite:

    slack_domain = "https://fortesting10.slack.com"

    # Login to Slack as User 1
    def test_login(self):
        get_webdriver().get(self.slack_domain)
        type_username("user1teamable@gmail.com")
        type_password_slack("Teamable123!@#")
        click_signin()

    # Invite User 2 to Slack
    def test_invite(self):
        wait_for_page_to_load()
        click_invite_people()
        type_email_by_index("0", "user2teamable@gmail.com")
        click_invite()

    # Login to Gmail as User 2
    def test_login_gmail(self):
        wait_for_page_to_load()
        get_webdriver().get("https://mail.google.com")
        type_email_gmail("user2teamable@gmail.com")
        click_next_username()
        type_password_gmail("Teamable123!@#\n")

    # Open the invitation and click Join
    def test_click_invitation(self):
        wait_for_page_to_load()
        select_email()
        wait_for_page_to_load()
        click_join_button()

    # Register to Slack as User 2
    def test_join_slack(self):
        handle = get_webdriver().window_handles[1]
        get_webdriver().switch_to.window(handle)
        type_full_name_join("User 2")
        type_password_join("Teamable123!@#")
        click_create_account()
        skip_invitations()

    expected_text = "Thanks for the invitation"

    # Send message to User 1
    def test_send_message(self):
        escape_welcome()
        send_message_by_name("User 1", self.expected_text + "\n")
        sign_out()

    # Login to Slack as User 1
    def test_login_back(self):
        get_webdriver().get(self.slack_domain)
        type_username("user1teamable@gmail.com")
        type_password_slack("Teamable123!@#")
        click_signin()

    # Read the message from User 2
    # Verify the message content
    def test_get_message(self):
        actual_text = get_message_by_name("User 2")

        assert actual_text == self.expected_text

    # Quit webdriver
    def test_tear_down(self):
        quit_webdriver()
