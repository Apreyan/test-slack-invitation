from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager


capabilities = {
    "browserName": "firefox",
    "version": "67.0",
    "enableVNC": True,
    "enableVideo": False
}

# driver = webdriver.Firefox(executable_path="D:/Levon/Projects/webdrivers/geckodriver.exe")
# driver = webdriver.Chrome(executable_path="D:/Levon/Projects/webdrivers/chromedriver.exe")
driver = webdriver.Remote(
    command_executor="http://35.239.55.68:4444/wd/hub",
    desired_capabilities=capabilities)


def get_webdriver():
    driver.set_page_load_timeout(60)
    # driver.implicitly_wait(60)
    return driver
    # driver.get("https://www.yahoo.com")


def set_webdriver(webdriver):
    driver = webdriver
    return driver


def quit_webdriver():
    driver.close()
    driver.quit()