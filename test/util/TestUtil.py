from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from test.util.WebdriverFactory import get_webdriver


def wait_element_to_be_clickable(css_selector):
    WebDriverWait(get_webdriver(), 60).until(
        EC.presence_of_element_located((By.CSS_SELECTOR, css_selector)))
    WebDriverWait(get_webdriver(), 60).until(
        EC.element_to_be_clickable((By.CSS_SELECTOR, css_selector)))


def wait_for_page_to_load():
    WebDriverWait(get_webdriver(), 30).until(
        EC.presence_of_element_located)
