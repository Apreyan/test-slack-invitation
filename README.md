# Test Slack Invitation

A proof of concept for a Pytest driven automated multi-browser testing environment on the cloud using dynamic resources (pay per use architecture) for the Test Scenario.

Please find the details about the implimentation in ./Test/Test Project.pdf file